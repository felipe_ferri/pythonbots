#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on Mar 16, 2017

@author: felipe
'''

import requests
from bs4 import BeautifulSoup
import re
import pprint

session = requests.Session()

session.verify = 'charles-ssl-proxying-certificate.pem'


cities_temps = {}

for city in range(244, 300):
    temps = []
    
    r = session.get('http://www.cptec.inpe.br/cidades/tempo/%d'%city)
    
    html = r.text
    soup = BeautifulSoup(html, 'html.parser')
    city_name = soup.title.string
    titles = [unicode(x.string) for x in soup.find_all('div', 'tit')]
    min_temp_htmls = soup.find_all('div', 'c2', 'b')[1:]
    min_temps = [''.join(min_temp_html.b.stripped_strings) for min_temp_html in min_temp_htmls]
    max_temp_htmls = soup.find_all('div', 'c3', 'b')[1:]
    max_temps = [''.join(max_temp_html.b.stripped_strings) for max_temp_html in max_temp_htmls]

    values = zip(titles, min_temps, max_temps)
    
    for date, min_temp, max_temp in values:
        temp = {'date': date,
                'min_temp': min_temp,
                'max_temp': max_temp}
        temps.append(temp)
    
    cities_temps[city_name] = temps
    
pprint.pprint(cities_temps)